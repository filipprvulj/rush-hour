﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RushHour.DataAccess
{
    public static class RoleName
    {
        public const string Admin = "Admin";
        public const string User = "User";
    }
}
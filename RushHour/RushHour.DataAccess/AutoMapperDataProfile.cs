﻿using AutoMapper;
using RushHour.DataAccess.Dtos;
using RushHour.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RushHour.DataAccess
{
    public class AutoMapperDataProfile : Profile
    {
        public AutoMapperDataProfile()
        {
            CreateMap<Activity, ActivityDto>();
            CreateMap<User, UserDto>();

            CreateMap<ActivityDto, Activity>()
                .ForMember(dest => dest.Id, opt => opt.Condition(src => src.Id != Guid.Empty));

            CreateMap<UserDto, User>()
                .ForMember(dest => dest.Id, opt => opt.Condition(src => src.Id != Guid.Empty));
        }
    }
}
﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RushHour.DataAccess.Migrations
{
    public partial class SetIdAsUniqueidentifier : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ActivityAppointment_Activities_ActivitiesId",
                table: "ActivityAppointment");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Activities",
                table: "Activities");

            migrationBuilder.DropForeignKey(
                name: "FK_ActivityAppointment_Appointments_AppointmentsId",
                table: "ActivityAppointment");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Appointments",
                table: "Appointments");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ActivityAppointment",
                table: "ActivityAppointment");

            migrationBuilder.DropIndex(
                name: "IX_ActivityAppointment_AppointmentsId",
                table: "ActivityAppointment");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Activities",
                newName: "OldId");

            migrationBuilder.AddColumn<Guid>(
                name: "Id",
                table: "Activities",
                type: "uniqueidentifier",
                defaultValueSql: "newid()");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Activities",
                table: "Activities",
                column: "Id");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Appointments",
                newName: "OldId");

            migrationBuilder.AddColumn<Guid>(
                name: "Id",
                table: "Appointments",
                type: "uniqueidentifier",
                defaultValueSql: "newid()");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Appointments",
                table: "Appointments",
                column: "Id");

            migrationBuilder.RenameColumn(
                name: "AppointmentsId",
                table: "ActivityAppointment",
                newName: "OldAppointmentsId"
                );

            migrationBuilder.AddColumn<Guid>(
                name: "AppointmentsId",
                table: "ActivityAppointment",
                type: "uniqueidentifier");

            migrationBuilder.RenameColumn(
                name: "ActivitiesId",
                table: "ActivityAppointment",
                newName: "OldActivitiesId");

            migrationBuilder.AddColumn<Guid>(
                name: "ActivitiesId",
                table: "ActivityAppointment",
                type: "uniqueidentifier");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ActivityAppointment",
                table: "ActivityAppointment",
                columns: new[] { "ActivitiesId", "AppointmentsId" });

            migrationBuilder.AddForeignKey(
                name: "FK_ActivityAppointment_Activities_ActivitiesId",
                table: "ActivityAppointment",
                column: "ActivitiesId",
                principalTable: "Activities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ActivityAppointment_Appointments_AppointmentsId",
                table: "ActivityAppointment",
                column: "AppointmentsId",
                principalTable: "Appointments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.CreateIndex(
               name: "IX_ActivityAppointment_AppointmentsId",
               table: "ActivityAppointment",
               column: "AppointmentsId");

            migrationBuilder.DropColumn(
                name: "OldId",
                table: "Activities");

            migrationBuilder.DropColumn(
                name: "OldId",
                table: "Appointments");

            migrationBuilder.DropColumn(
                name: "OldAppointmentsId",
                table: "ActivityAppointment");

            migrationBuilder.DropColumn(
                name: "OldActivitiesId",
                table: "ActivityAppointment");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Id",
                table: "Appointments",
                type: "nvarchar(450)",
                nullable: false,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier");

            migrationBuilder.AlterColumn<string>(
                name: "AppointmentsId",
                table: "ActivityAppointment",
                type: "nvarchar(450)",
                nullable: false,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier");

            migrationBuilder.AlterColumn<string>(
                name: "ActivitiesId",
                table: "ActivityAppointment",
                type: "nvarchar(450)",
                nullable: false,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier");

            migrationBuilder.AlterColumn<string>(
                name: "Id",
                table: "Activities",
                type: "nvarchar(450)",
                nullable: false,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier");
        }
    }
}
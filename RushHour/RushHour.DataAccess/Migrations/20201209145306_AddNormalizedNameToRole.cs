﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RushHour.DataAccess.Migrations
{
    public partial class AddNormalizedNameToRole : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("UPDATE AspNetRoles SET NormalizedName = 'USER' WHERE Name = 'User'");
            migrationBuilder.Sql("UPDATE AspNetRoles SET NormalizedName = 'ADMIN' WHERE Name = 'Admin'");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}
﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace RushHour.DataAccess.Migrations
{
    public partial class AddUserRoles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql($"INSERT INTO AspNetRoles (Id, Name) VALUES ('{Guid.NewGuid().ToString()}','Admin')");
            migrationBuilder.Sql($"INSERT INTO AspNetRoles (Id, Name) VALUES ('{Guid.NewGuid().ToString()}','User')");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}
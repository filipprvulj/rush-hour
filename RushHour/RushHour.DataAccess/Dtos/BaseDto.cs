﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RushHour.DataAccess.Dtos
{
    public class BaseDto
    {
        public Guid Id { get; set; }
    }
}
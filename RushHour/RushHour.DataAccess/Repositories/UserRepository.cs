﻿using RushHour.DataAccess.DataAccess;
using RushHour.DataAccess.Entities;
using RushHour.DataAccess.Repositories.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using RushHour.DataAccess.Dtos;
using AutoMapper;

namespace RushHour.DataAccess.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;

        public UserRepository(UserManager<User> userManager, IMapper mapper)
        {
            _userManager = userManager;
            _mapper = mapper;
        }

        public async Task<UserDto> GetUserByEmailAsync(string email)
        {
            User user = await _userManager.FindByEmailAsync(email);

            return _mapper.Map<UserDto>(user);
        }

        public Task<bool> CheckPasswordAsync(UserDto userDto, string password)
        {
            User user = _mapper.Map<User>(userDto);
            return _userManager.CheckPasswordAsync(user, password);
        }

        public Task<IList<string>> GetUserRolesAsync(UserDto userDto)
        {
            User user = _mapper.Map<User>(userDto);

            return _userManager.GetRolesAsync(user);
        }

        public async Task<IdentityResult> RegisterUser(UserDto userDto, string password)
        {
            User user = _mapper.Map<User>(userDto);
            IdentityResult created = await _userManager.CreateAsync(user, password);
            if (created.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, RoleName.User);
            }

            return created;
        }
    }
}
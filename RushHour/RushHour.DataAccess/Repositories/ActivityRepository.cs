﻿using AutoMapper;
using RushHour.DataAccess.DataAccess;
using RushHour.DataAccess.Dtos;
using RushHour.DataAccess.Entities;
using RushHour.DataAccess.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RushHour.DataAccess.Repositories
{
    public class ActivityRepository : BaseRepository<ActivityDto, Activity>, IActivityRepository
    {
        public ActivityRepository(ApplicationDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}
﻿using RushHour.DataAccess.DataAccess;
using RushHour.DataAccess.Entities;
using RushHour.DataAccess.Repositories.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RushHour.DataAccess.Dtos;
using AutoMapper;
using System;

namespace RushHour.DataAccess.Repositories
{
    public abstract class BaseRepository<TDto, TEntity> : IBaseRepository<TDto>
       where TDto : BaseDto
       where TEntity : BaseEntity
    {
        protected readonly ApplicationDbContext _context;
        protected readonly IMapper _mapper;

        protected DbSet<TEntity> Entity { get; set; }

        protected BaseRepository(ApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
            Entity = _context.Set<TEntity>();
        }

        public Task<int> AddAsync(TDto dto)
        {
            var entity = _mapper.Map<TEntity>(dto);
            Entity.Add(entity);
            return _context.SaveChangesAsync();
        }

        public Task<TDto> GetByIdAsync(Guid id, bool detach)
        {
            var entities = (detach) ? Entity.AsNoTracking() : Entity;

            return _mapper.ProjectTo<TDto>(entities).FirstOrDefaultAsync(e => e.Id == id);
        }

        public Task<List<TDto>> GetPaginatedAsync(int page, int itemCount)
        {
            var entities = Entity.Skip((page - 1) * itemCount).Take(itemCount);
            return _mapper.ProjectTo<TDto>(entities).ToListAsync();
        }

        public Task<int> DeleteAsync(TDto dto)
        {
            var entity = _mapper.Map<TEntity>(dto);
            Entity.Remove(entity);
            return _context.SaveChangesAsync();
        }

        public async Task<int> UpdateAsync(TDto dto, Guid id)
        {
            var existing = await GetExistingByIdAsync(id);
            var mapper = _mapper.Map(dto, existing);

            return await _context.SaveChangesAsync();
        }

        private Task<TEntity> GetExistingByIdAsync(Guid id)
        {
            return Entity.FirstOrDefaultAsync(e => e.Id == id);
        }
    }
}
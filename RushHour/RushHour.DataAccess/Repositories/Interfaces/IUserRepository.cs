﻿using RushHour.DataAccess.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RushHour.DataAccess.Dtos;

namespace RushHour.DataAccess.Repositories.Interfaces
{
    public interface IUserRepository
    {
        public Task<IdentityResult> RegisterUser(UserDto userDto, string password);

        public Task<UserDto> GetUserByEmailAsync(string email);

        public Task<bool> CheckPasswordAsync(UserDto userDto, string password);

        public Task<IList<string>> GetUserRolesAsync(UserDto userDto);
    }
}
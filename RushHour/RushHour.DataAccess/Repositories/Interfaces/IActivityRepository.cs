﻿using RushHour.DataAccess.Dtos;
using RushHour.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RushHour.DataAccess.Repositories.Interfaces
{
    public interface IActivityRepository : IBaseRepository<ActivityDto>
    {
    }
}
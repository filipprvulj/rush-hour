﻿using RushHour.DataAccess.Dtos;
using RushHour.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RushHour.DataAccess.Repositories.Interfaces
{
    public interface IBaseRepository<TDto> where TDto : BaseDto
    {
        public Task<int> AddAsync(TDto dto);

        public Task<TDto> GetByIdAsync(Guid id, bool detach);

        public Task<List<TDto>> GetPaginatedAsync(int page, int itemCount);

        public Task<int> DeleteAsync(TDto dto);

        public Task<int> UpdateAsync(TDto dto, Guid id);
    }
}
﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RushHour.DataAccess.Entities
{
    public class User : IdentityUser
    {
        public List<Appointment> Appointments { get; set; }
    }
}
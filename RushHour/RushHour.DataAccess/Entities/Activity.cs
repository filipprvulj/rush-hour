﻿using System.Collections.Generic;

namespace RushHour.DataAccess.Entities
{
    public class Activity : BaseEntity
    {
        public string Name { get; set; }
        public int DurationInMinutes { get; set; }
        public decimal Price { get; set; }
        public List<Appointment> Appointments { get; set; }
    }
}
﻿using RushHour.Service.Services.Interfaces;
using RushHour.DataAccess;
using RushHour.DataAccess.Entities;
using RushHour.DataAccess.Repositories.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using RushHour.DataAccess.Dtos;

namespace RushHour.Service.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _repository;
        private readonly JwtSettingsOptions _options;

        public UserService(IUserRepository repository, IOptions<JwtSettingsOptions> options)
        {
            _repository = repository;
            _options = options.Value;
        }

        public async Task<Result> RegisterUserAsync(string username, string email, string password)
        {
            Result result = new Result();
            UserDto user = new UserDto()
            {
                Email = email,
                UserName = username
            };

            var created = await _repository.RegisterUser(user, password);
            if (created.Succeeded)
            {
                result.Succeeded = true;
                return result;
            }
            else
            {
                result.Errors = created.Errors.Select(e => e.Description).ToList();
                return result;
            }
        }

        public async Task<Result<string>> LoginUserAsync(string email, string password)
        {
            Result<string> result = new Result<string>();
            var user = await _repository.GetUserByEmailAsync(email);

            if (user != default)
            {
                var isPasswordValid = await _repository.CheckPasswordAsync(user, password);
                if (isPasswordValid)
                {
                    result.Succeeded = true;
                    result.Value = await GenerateJWT(user);
                    return result;
                }
                else
                {
                    result.Errors.Add("Invalid passowrd");
                    return result;
                }
            }
            else
            {
                result.Errors.Add("User not found");
                return result;
            }
        }

        private async Task<string> GenerateJWT(UserDto user)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_options.Key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            List<Claim> claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                new Claim(JwtRegisteredClaimNames.Iat, new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds().ToString()),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var userRoles = await _repository.GetUserRolesAsync(user);
            if (userRoles.Contains(RoleName.User))
            {
                claims.Add(new Claim(ClaimTypes.Role, RoleName.User));
            }
            else if (userRoles.Contains(RoleName.Admin))
            {
                claims.Add(new Claim(ClaimTypes.Role, RoleName.Admin));
            }

            var token = new JwtSecurityToken(
                issuer: _options.Issuer,
                audience: _options.Audience,
                claims: claims,
                expires: DateTime.UtcNow.AddMinutes(_options.ExpirationInMinutes),
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
﻿using RushHour.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RushHour.Service.Services.Interfaces
{
    public interface IUserService
    {
        public Task<Result> RegisterUserAsync(string username, string email, string password);

        public Task<Result<string>> LoginUserAsync(string email, string password);
    }
}
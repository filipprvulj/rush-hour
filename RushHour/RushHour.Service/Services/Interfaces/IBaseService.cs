﻿using RushHour.DataAccess.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RushHour.Service.Services.Interfaces
{
    public interface IBaseService<TDto> where TDto : BaseDto
    {
        public Task<Result> AddAsync(TDto entity);

        public Task<Result<List<TDto>>> GetPaginatedAsync(int page, int itemCount);

        public Task<Result<TDto>> GetByIdAsync(Guid id);

        public Task<Result> DeleteAsync(Guid id);

        public Task<Result> UpdateAsync(Guid id, TDto submitted);
    }
}
﻿using RushHour.DataAccess.Dtos;
using RushHour.DataAccess.Entities;
using RushHour.DataAccess.Repositories.Interfaces;
using RushHour.Service.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RushHour.Service.Services
{
    public class ActivityService : BaseService<ActivityDto, IActivityRepository>, IActivityService
    {
        public ActivityService(IActivityRepository repository) : base(repository)
        {
        }

        //public override Task<Result> AddAsync(ActivityDto entity)
        //{
        //    entity.Id = Guid.NewGuid().ToString();
        //    return base.AddAsync(entity);
        //}
    }
}
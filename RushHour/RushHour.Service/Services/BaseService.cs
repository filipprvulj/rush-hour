﻿using RushHour.DataAccess.Dtos;
using RushHour.DataAccess.Entities;
using RushHour.DataAccess.Repositories.Interfaces;
using RushHour.Service.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RushHour.Service.Services
{
    public abstract class BaseService<TDto, TRepository> : IBaseService<TDto>
        where TDto : BaseDto
        where TRepository : IBaseRepository<TDto>
    {
        protected readonly IBaseRepository<TDto> _repository;

        protected BaseService(TRepository repository)
        {
            _repository = repository;
        }

        public virtual async Task<Result> AddAsync(TDto entity)
        {
            Result result = new Result();
            if (entity is null)
            {
                result.Errors.Add($"{typeof(TDto).Name} must be provided");

                return result;
            }

            var added = await _repository.AddAsync(entity);
            if (added != 0)
            {
                result.Succeeded = true;
                return result;
            }
            else
            {
                result.Errors.Add("Insert failed");
                return result;
            }
        }

        public async Task<Result> DeleteAsync(Guid id)
        {
            Result result = new Result();

            if (id == Guid.Empty)
            {
                result.Errors.Add("You must provide id");
                return result;
            }

            var item = await _repository.GetByIdAsync(id, true);

            if (item == null)
            {
                result.Errors.Add($"{typeof(TDto).Name} with id '{id}' is not found");
                return result;
            }
            else
            {
                var deleteItem = await _repository.DeleteAsync(item);
                if (deleteItem != 0)
                {
                    result.Succeeded = true;
                    return result;
                }
                else
                {
                    result.Errors.Add("Remove failed");
                    return result;
                }
            }
        }

        public async Task<Result<TDto>> GetByIdAsync(Guid id)
        {
            Result<TDto> result = new Result<TDto>();

            if (id == Guid.Empty)
            {
                result.Errors.Add("You must provide id");
                return result;
            }

            var item = await _repository.GetByIdAsync(id, false);

            if (item == null)
            {
                result.Errors.Add($"{typeof(TDto).Name} with id '{id}' is not found");
                return result;
            }
            else
            {
                result.Succeeded = true;
                result.Value = item;
                return result;
            }
        }

        public async Task<Result<List<TDto>>> GetPaginatedAsync(int page, int itemCount)
        {
            Result<List<TDto>> result = new Result<List<TDto>>();
            var items = await _repository.GetPaginatedAsync(page, itemCount);

            if (!items.Any())
            {
                result.Errors.Add("Page doesn't exist");
                return result;
            }
            else
            {
                result.Succeeded = true;
                result.Value = items;
                return result;
            }
        }

        public async Task<Result> UpdateAsync(Guid id, TDto submitted)
        {
            Result result = new Result();
            var dto = await _repository.GetByIdAsync(id, true);
            if (dto == default)
            {
                result.Errors.Add("Not found");
                return result;
            }

            var updated = await _repository.UpdateAsync(submitted, id);
            if (updated != default)
            {
                result.Succeeded = true;
                return result;
            }
            else
            {
                result.Errors.Add("Update failed");
                return result;
            }
        }
    }
}
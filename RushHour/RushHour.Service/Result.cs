﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RushHour.Service
{
    public class Result<T>
    {
        public List<string> Errors { get; set; } = new List<string>();
        public bool Succeeded { get; set; } = false;
        public T Value { get; set; }
    }

    public class Result
    {
        public List<string> Errors { get; set; } = new List<string>();
        public bool Succeeded { get; set; } = false;
    }
}
﻿using AutoMapper;
using RushHour.DataAccess.Dtos;
using RushHour.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RushHour.Service
{
    public class AutoMapperServiceProfile : Profile
    {
        public AutoMapperServiceProfile()
        {
            CreateMap<Activity, ActivityDto>();

            CreateMap<ActivityDto, Activity>()
                .ForMember(dest => dest.Id, opt => opt.Condition(src => src.Id != Guid.Empty));
        }
    }
}
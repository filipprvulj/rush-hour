﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RushHour.DataAccess.Dtos;
using RushHour.Service.Services.Interfaces;
using RushHour.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RushHour.WebAPI.Controllers
{
    [Authorize(Policy = "RequireAdminRole")]
    [Route("api/[controller]")]
    [ApiController]
    public class ActivityController : ControllerBase
    {
        private readonly IActivityService _activityService;
        private readonly IMapper _mapper;

        public ActivityController(IActivityService activityService, IMapper mapper)
        {
            _activityService = activityService;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<IActionResult> AddActivity([FromBody] ActivityPost activity)
        {
            var activityDto = _mapper.Map<ActivityDto>(activity);
            var result = await _activityService.AddAsync(activityDto);
            if (result.Succeeded)
            {
                return Ok();
            }
            else
            {
                return BadRequest(result);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetActivitiesPaginated(int page, int itemCount)
        {
            var result = await _activityService.GetPaginatedAsync(page, itemCount);
            if (result.Succeeded)
            {
                return Ok(result.Value);
            }
            else
            {
                return BadRequest(result.Errors);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(Guid id)
        {
            var result = await _activityService.GetByIdAsync(id);
            if (result.Succeeded)
            {
                return Ok(result);
            }
            else
            {
                return BadRequest(result);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var result = await _activityService.DeleteAsync(id);
            if (result.Succeeded)
            {
                return Ok();
            }
            else
            {
                return BadRequest(result);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(Guid id, ActivityPost activity)
        {
            var activityDto = _mapper.Map<ActivityDto>(activity);
            var result = await _activityService.UpdateAsync(id, activityDto);
            if (result.Succeeded)
            {
                return Ok();
            }
            else
            {
                return BadRequest(result);
            }
        }
    }
}
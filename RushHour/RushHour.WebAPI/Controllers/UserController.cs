﻿using RushHour.Service.Services.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RushHour.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RushHour.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost("Register")]
        public async Task<IActionResult> Register([FromBody] RegisterUser user)
        {
            var registerUser = await _userService.RegisterUserAsync(user.Username, user.Email, user.Password);
            if (registerUser.Succeeded)
            {
                return Ok(registerUser);
            }
            else
            {
                return BadRequest(registerUser);
            }
        }

        [HttpPost("Login")]
        public async Task<IActionResult> Login([FromBody] LoginUser user)
        {
            var loginUser = await _userService.LoginUserAsync(user.Email, user.Password);
            if (loginUser.Succeeded)
            {
                return Ok(loginUser);
            }
            else
            {
                return BadRequest(loginUser);
            }
        }
    }
}
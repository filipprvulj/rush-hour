﻿using AutoMapper;
using RushHour.DataAccess.Dtos;
using RushHour.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RushHour.WebAPI
{
    public class AutoMapperControllerProfile : Profile
    {
        public AutoMapperControllerProfile()
        {
            CreateMap<ActivityPost, ActivityDto>();
        }
    }
}
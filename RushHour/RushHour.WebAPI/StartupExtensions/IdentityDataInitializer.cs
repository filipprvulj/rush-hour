﻿using RushHour.DataAccess;
using RushHour.DataAccess.DataAccess;
using RushHour.DataAccess.Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Configuration;

namespace RushHour.StartupExtensions
{
    public static class IdentityDataInitializer
    {
        public static void UseSeedData(this IApplicationBuilder app, UserManager<User> userManager, RoleManager<IdentityRole> roleManager, IConfiguration configuration)
        {
            Task.WaitAll(
                SeedRolesAsync(roleManager),
                SeedUsersAsync(userManager, configuration)
            );
        }

        private async static Task SeedRolesAsync(RoleManager<IdentityRole> roleManager)
        {
            var roleNames = new[] { RoleName.Admin, RoleName.User };
            foreach (var roleName in roleNames)
            {
                await CreateRole(roleManager, roleName);
            }
        }

        private async static Task CreateRole(RoleManager<IdentityRole> roleManager, string roleName)
        {
            if (!await roleManager.RoleExistsAsync(roleName))
            {
                IdentityRole role = new IdentityRole()
                {
                    Name = roleName
                };

                await roleManager.CreateAsync(role);
            }
        }

        private async static Task SeedUsersAsync(UserManager<User> userManager, IConfiguration configuration)
        {
            var adminUsers = await userManager.GetUsersInRoleAsync(RoleName.Admin);

            if (!adminUsers.Any())
            {
                User user = new User()
                {
                    UserName = configuration["Admin:Username"],
                    Email = configuration["Admin:Email"]
                };
                var result = await userManager.CreateAsync(user, configuration["Admin:Password"]);
                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(user, RoleName.Admin);
                }
            }
        }
    }
}